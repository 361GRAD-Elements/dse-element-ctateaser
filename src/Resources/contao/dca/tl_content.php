<?php

/**
 * 361GRAD Element Ctateaser
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

// Element palettes
$GLOBALS['TL_DCA']['tl_content']['palettes']['dse_ctateaser'] =
    '{type_legend},type;' .
    '{headline_legend},headline,dse_subheadline;' .
    '{text_legend},text;' .
    '{image_legend},addImage,dse_isBgOverlay;' .
    '{link_legend},url,target,dse_linkTitle,dse_titleText,dse_buttontype;' .
    '{margin_legend},dse_marginTop,dse_marginBottom;' .
    '{custom_legend},dse_isScrollbutton,dse_scrollButtonId;' .
    '{invisible_legend:hide},invisible,start,stop';

// Element subpalettes
$GLOBALS['TL_DCA']['tl_content']['subpalettes']['addImage'] = 'singleSRC,alt,title,size,imagemargin,fullsize,floating';

// Element fields
$GLOBALS['TL_DCA']['tl_content']['fields']['floating']['options']       = ['left', 'right'];
$GLOBALS['TL_DCA']['tl_content']['fields']['url']['eval']['mandatory']  = false;
$GLOBALS['TL_DCA']['tl_content']['fields']['text']['eval']['mandatory'] = false;

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_subheadline'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_subheadline'],
    'search'    => true,
    'inputType' => 'inputUnit',
    'options'   => [
        'h2',
        'h3',
        'h4',
        'h5',
        'h6'
    ],
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_linkTitle'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_linkTitle'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_titleText'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_titleText'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_buttontype'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_buttontype'],
    'default'   => 'none',
    'inputType' => 'radio',
    'options'   => [
        'none',
        'black',
        'grey',
        'white',
        'transparent'
    ],
    'reference' => [
        'none'        => Dse\ElementsBundle\ElementCtateaser\Element\ContentDseCtateaser::refColor(
            '#FFF',
            '#000',
            'Keine'
        ),
        'black'       => Dse\ElementsBundle\ElementCtateaser\Element\ContentDseCtateaser::refColor(
            '#141312',
            '#FFF',
            'Black'
        ),
        'grey'        => Dse\ElementsBundle\ElementCtateaser\Element\ContentDseCtateaser::refColor(
            '#C6C4C2',
            '#000',
            'LightGrey'
        ),
        'white'       => Dse\ElementsBundle\ElementCtateaser\Element\ContentDseCtateaser::refColor(
            '#F7F7F7',
            '#000',
            'NaturalWhite'
        ),
        'transparent' => Dse\ElementsBundle\ElementCtateaser\Element\ContentDseCtateaser::refColor(
            '#FFF',
            '#000',
            'Transparent'
        )
    ],
    'eval'      => [
        'tl_class' => 'clr'
    ],
    'sql'       => "varchar(32) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_isScrollbutton'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_isScrollbutton'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class'  => 'clr w50 m12',
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_scrollButtonId'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_scrollButtonId'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_isBgOverlay'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_isBgOverlay'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class'  => 'w50 m12',
    ],
    'sql'       => "blob NULL"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_marginTop']    = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_marginTop'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'sql'       => "text NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_marginBottom'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_marginBottom'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'sql'       => "text NULL"
];
