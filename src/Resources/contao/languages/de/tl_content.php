<?php

/**
 * 361GRAD Element Image-Text
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

$GLOBALS['TL_LANG']['CTE']['dse_elements'] = 'DSE-Elements';
$GLOBALS['TL_LANG']['CTE']['dse_ctateaser'] = ['CTA Teaser', 'CTA Teaser mit Text und Link (optional)'];

$GLOBALS['TL_LANG']['tl_content']['headline_legend']   = 'Schlagzeileneinstellungen';
$GLOBALS['TL_LANG']['tl_content']['dse_subheadline']  =
    ['Unter-Überschrift', 'Hier können Sie eine Subheadline hinzufügen.'];

$GLOBALS['TL_LANG']['tl_content']['dse_isBgOverlay']  =
    ['Verwenden Sie Black Transparency Overlay', 'Hier können Sie überprüfen, ob das Hintergrundbild eine transparente Überlagerung hat.'];

$GLOBALS['TL_LANG']['tl_content']['dse_linkTitle']  =
    ['Link Titel', 'Hier kannst du Link Titel hinzufügen.'];
$GLOBALS['TL_LANG']['tl_content']['dse_titleText']  =
    ['Link Text', 'Hier können Sie Link Text hinzufügen.'];    
$GLOBALS['TL_LANG']['tl_content']['dse_buttontype']  =
    ['Knopffarbe', 'Hier kannst du Button Type ändern.'];

$GLOBALS['TL_LANG']['tl_content']['custom_legend']   = 'Benutzerdefinierte Einstellungen';
$GLOBALS['TL_LANG']['tl_content']['dse_isScrollbutton']  =
    ['Blättern Sie nach unten', 'Ist das ein Element mit der Scroll-Down-Taste?'];
$GLOBALS['TL_LANG']['tl_content']['dse_scrollButtonId']  =
    ['Blättern Sie zu ID', 'Verwenden Sie dieses Feld, um die ID hinzuzufügen, auf die die Seite gescrollt wird.'];

    $GLOBALS['TL_LANG']['tl_content']['margin_legend']   = 'Randeinstellungen';
$GLOBALS['TL_LANG']['tl_content']['dse_marginTop']   = ['Rand oben', 'Hier können Sie Margin zum oberen Rand des Elements hinzufügen (nur nummern)'];
$GLOBALS['TL_LANG']['tl_content']['dse_marginBottom']   = ['Rand unten', 'Hier können Sie dem unteren Rand des Elements Rand hinzufügen (nur nummern)'];