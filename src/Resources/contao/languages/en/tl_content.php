<?php

/**
 * 361GRAD Element Image-Text
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

$GLOBALS['TL_LANG']['CTE']['dse_elements'] = 'DSE-Elements';
$GLOBALS['TL_LANG']['CTE']['dse_ctateaser'] = ['CTA Teaser', 'CTA Teaser with text and link (optional)'];

$GLOBALS['TL_LANG']['tl_content']['headline_legend']   = 'Headline settings';
$GLOBALS['TL_LANG']['tl_content']['dse_subheadline']  =
    ['Subheadline', 'Here you can add a Subheadline.'];

$GLOBALS['TL_LANG']['tl_content']['dse_isBgOverlay']  =
    ['Use Black Transparency Overlay', 'Here you can check whether background image will have transparent overlay.'];

$GLOBALS['TL_LANG']['tl_content']['dse_linkTitle']  =
    ['Link Title', 'Here you can add Link Title.'];
$GLOBALS['TL_LANG']['tl_content']['dse_titleText']  =
    ['Link Text', 'Here you can add Link Text.'];    
$GLOBALS['TL_LANG']['tl_content']['dse_buttontype']  =
    ['Button Color', 'Here you can change Button Type.'];

$GLOBALS['TL_LANG']['tl_content']['custom_legend']   = 'Custom settings';
$GLOBALS['TL_LANG']['tl_content']['dse_isScrollbutton']  =
    ['Scroll Down button', 'Is this a element has Scroll Down button?'];
$GLOBALS['TL_LANG']['tl_content']['dse_scrollButtonId']  =
    ['Scroll to ID', 'Use this field to add the ID to which the page will be scrolled' ];

$GLOBALS['TL_LANG']['tl_content']['margin_legend']   = 'Margin Settings';
$GLOBALS['TL_LANG']['tl_content']['dse_marginTop']   = ['Margin Top', 'Here you can add Margin to the top edge of the element (numbers only)'];
$GLOBALS['TL_LANG']['tl_content']['dse_marginBottom']   = ['Margin Bottom', 'Here you can add Margin to the bottom edge of the element (numbers only)'];